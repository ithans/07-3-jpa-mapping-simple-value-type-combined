package com.twuc.webApp.domain.composite;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepo extends JpaRepository<CompanyProfile,Long> {
}
