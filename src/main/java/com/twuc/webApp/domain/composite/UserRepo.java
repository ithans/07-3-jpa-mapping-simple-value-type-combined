package com.twuc.webApp.domain.composite;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<UserProfile,Long> {
}
