package com.twuc.webApp.domain.composite;

import javax.persistence.*;

@Entity
@Table(name = "user_profile")
public class UserProfile {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false,length = 128,name = "address_city")
    private String addressCity;
    @Column(nullable = false,length = 128,name = "address_street")
    private String addressStreet;

    public UserProfile() {
    }

    public UserProfile(String addressCity, String addressStreet) {
        this.addressCity = addressCity;
        this.addressStreet = addressStreet;
    }

    public Long getId() {
        return id;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public String getAddressStreet() {
        return addressStreet;
    }
}
