package com.twuc.webApp.domain.composite;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
@DataJpaTest(showSql = false)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
class SimpleMappingAndValueTest {
    @Autowired
    private CompanyRepo companyRepo;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private EntityManager em;

    @Test
    void should_save_company_entity_and_select() {
        CompanyProfile companyProfile = companyRepo.save(new CompanyProfile("wuhan", "guangminglu"));
        em.flush();
        Assertions.assertNotNull(companyProfile);
        CompanyProfile companyProfile1 = em.find(CompanyProfile.class,companyProfile.getId() );
        Assertions.assertEquals("wuhan",companyProfile1.getCity());
    }

    @Test
    void should_save_user_entity_and_select() {
        UserProfile user = userRepo.save(new UserProfile("wuhan", "guangminglu"));
        em.flush();
        Assertions.assertNotNull(user);
        UserProfile userProfile = em.find(UserProfile.class,user.getId() );
        Assertions.assertEquals("wuhan",userProfile.getAddressCity());
    }
}